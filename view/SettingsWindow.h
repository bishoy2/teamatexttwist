#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <Fl/Fl_Input.H>
#include "OKCancelWindow.h"
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Check_Button.H>
#include <vector>

#include <Settings.h>
#include <SettingsIO.h>
using namespace controller;

using namespace std;
using namespace model;

namespace view {

class SettingsWindow : public OKCancelWindow
{
    public:
        SettingsWindow();
        virtual ~SettingsWindow();

        static void cbRadioButton(Fl_Widget* widget, void* data);

        void okHandler();
        void cancelHandler();

        Settings* getSettings();


    private:
        Fl_Round_Button* oneMinuteRadioButton;
        Fl_Round_Button* twoMinuteRadioButton;
        Fl_Round_Button* threeMinuteRadioButton;

        Fl_Check_Button* reuseLettersCheckBox;

        vector<Fl_Round_Button*> radioButtons;

        Settings* gameSettings;

        SettingsIO* settingsFileIO;

        void displaySettings();
};
}
#endif // SETTINGSWINDOW_H
