#ifndef HIGHSCOREWINDOW_H
#define HIGHSCOREWINDOW_H

#include <Fl/Fl_Window.H>
#include <Fl/Fl_Button.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <OKCancelWindow.h>

#include <HighScoreManager.h>
using namespace controller;

namespace view
{

class HighScoreWindow : public OKCancelWindow
{

private:

    Fl_Text_Buffer *outputTextBuffer;
    Fl_Text_Display *outputTextDisplay;

    HighScoreManager* highScoreManager;

public:
    HighScoreWindow();
    virtual ~HighScoreWindow();

    void okHandler();
    void cancelHandler();

    void displayScores();

};

}

#endif // HIGHSCOREWINDOW_H
