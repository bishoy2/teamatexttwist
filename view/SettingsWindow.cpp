#include "SettingsWindow.h"

namespace view {

/**
    Constructor for the settings window.
*/
SettingsWindow::SettingsWindow() : OKCancelWindow(450, 250, "TextTwist! Settings")
{
    this->gameSettings = 0;

    begin();

    this->oneMinuteRadioButton = new Fl_Round_Button(25, 25, 12, 12, "1 minute");
    this->oneMinuteRadioButton->callback(cbRadioButton, this);
    //Change the default button selection here when FileIO is worked in
    this->oneMinuteRadioButton->value(1);
    this->twoMinuteRadioButton = new Fl_Round_Button(25, 50, 12, 12, "2 minutes");
    this->twoMinuteRadioButton->callback(cbRadioButton, this);
    this->threeMinuteRadioButton = new Fl_Round_Button(25, 75, 12, 12, "3 minutes");
    this->threeMinuteRadioButton->callback(cbRadioButton, this);

    this->radioButtons.push_back(this->oneMinuteRadioButton);
    this->radioButtons.push_back(this->twoMinuteRadioButton);
    this->radioButtons.push_back(this->threeMinuteRadioButton);

    this->reuseLettersCheckBox = new Fl_Check_Button(25, 125, 12, 12, "Reuse letter tiles");

    this->setOKLocation(140, 200);
    this->setCancelLocation(220, 200);

    end();

    this->settingsFileIO = new SettingsIO();
    this->gameSettings = settingsFileIO->readInSettings();
    this->displaySettings();
}

/**
    Click handler for the radio buttons.
*/
void SettingsWindow::cbRadioButton(Fl_Widget* widget, void* data)
{
    SettingsWindow* window = (SettingsWindow*)data;
    Fl_Round_Button* currentButton = (Fl_Round_Button*)widget;
    window->oneMinuteRadioButton->value(0);
    window->twoMinuteRadioButton->value(0);
    window->threeMinuteRadioButton->value(0);
    currentButton->value(1);
}

/**
    Handles the response of the user clicking 'OK'
*/
void SettingsWindow::okHandler()
{
    int numberOfGameMinutes = 1;
    bool reuseLetters = this->reuseLettersCheckBox->value();

    for(int i = 0; i < this->radioButtons.size(); i++)
    {
        Fl_Round_Button* currentButton = this->radioButtons[i];
        if(currentButton->value() == 1)
        {
            numberOfGameMinutes = i + 1;
        }
    }

    this->gameSettings = new Settings(numberOfGameMinutes, reuseLetters);
    this->settingsFileIO->saveSettings(gameSettings);

    this->hide();
}

/**
    Handles the response of the user clicking 'Cancel'
*/
void SettingsWindow::cancelHandler()
{
    if (this->gameSettings)
    {
        delete gameSettings;
    }

    this->gameSettings = 0;
    this->hide();
}

/**
    Gets the Settings object built from the gui selections.
*/
Settings* SettingsWindow::getSettings()
{
    return this->gameSettings;
}

/**
    Updates the display with the settings loaded.
*/
void SettingsWindow::displaySettings()
{
    this->oneMinuteRadioButton->value(0);
    this->twoMinuteRadioButton->value(0);
    this->threeMinuteRadioButton->value(0);
    if (this->gameSettings->numberOfMinutesInGame == 1)
    {
        this->oneMinuteRadioButton->value(1);
    }
    else if (this->gameSettings->numberOfMinutesInGame == 2)
    {
        this->twoMinuteRadioButton->value(1);
    }
    else if (this->gameSettings->numberOfMinutesInGame == 3)
    {
        this->threeMinuteRadioButton->value(1);
    }

    if (this->gameSettings->reuseLetters)
    {
        this->reuseLettersCheckBox->value(1);
    }
    else
    {
        this->reuseLettersCheckBox->value(0);
    }

}

SettingsWindow::~SettingsWindow()
{
    delete this->oneMinuteRadioButton;
    delete this->twoMinuteRadioButton;
    delete this->threeMinuteRadioButton;

    delete this->reuseLettersCheckBox;

    delete this->gameSettings;
    delete this->settingsFileIO;
}

}
