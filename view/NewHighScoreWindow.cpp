#include "NewHighScoreWindow.h"

namespace view
{

NewHighScoreWindow::NewHighScoreWindow() : OKCancelWindow(400, 150, "New High Score! Enter your name...")
{
    begin();

    this->playerNameInput = new Fl_Input(110, 15, 200, 25, "Player name:");
    this->setOKLocation(150, 50);
    this->setCancelLocation(700, 700);
    this->playerName = "";

    end();
}

/**
    Destructor for this object.
*/
NewHighScoreWindow::~NewHighScoreWindow()
{
    delete this->playerNameInput;
}

/**
    Handles the clicking of the Ok button
*/
void NewHighScoreWindow::okHandler()
{
    try
    {
        this->validateInput();

        this->playerName = this->playerNameInput->value();

        this->hide();
    }
    catch (const char* message)
    {
        fl_message("%s", message);
    }
}

/**
    Validates name input.
*/
void NewHighScoreWindow::validateInput()
{
    std::string name = this->playerNameInput->value();
    if (name.compare("") == 0)
    {
        throw "Name cannot be blank.";
    }
}

/**
    The instance handler when Cancel is invoked

    @precondition none
    @postcondition getMovieNmme() == ""
*/
void NewHighScoreWindow::cancelHandler()
{
    this->playerName = "";
    this->hide();
}

/**
    Gets the player name entered.
*/
const std::string NewHighScoreWindow::getPlayerName() const
{
    return this->playerName;
}

}
