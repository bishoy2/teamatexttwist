#include "HighScoreWindow.h"

namespace view
{

/**
    Constructor for this object.
*/
HighScoreWindow::HighScoreWindow() : OKCancelWindow(450, 250, "High Scores")
{
    begin();

    this->outputTextBuffer = new Fl_Text_Buffer();
    this->outputTextDisplay = new Fl_Text_Display(20, 20, 400, 170);
    this->outputTextDisplay->textfont(FL_COURIER);
    this->outputTextDisplay->buffer(outputTextBuffer);
    this->outputTextDisplay->label("High Scores:");

    this->setOKLocation(140, 200);
    this->setCancelLocation(50000, 50000);

    end();

    this->highScoreManager = new HighScoreManager();
    this->displayScores();
}

/**
    Destructor for this object.
*/
HighScoreWindow::~HighScoreWindow()
{
    //dtor
}

/**
    Handles the clicking of the ok button
*/
void HighScoreWindow::okHandler()
{
    this->hide();
}

void HighScoreWindow::cancelHandler()
{

}

/**
    Displays the HighScores saved to file.
*/
void HighScoreWindow::displayScores()
{
    this->highScoreManager->loadHighScores();
    std::string outputText = this->highScoreManager->getHighScoreText();
    this->outputTextBuffer->text(outputText.c_str());
}

}
