#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Output.H>
#include <string>
#include "GameManager.h"
#include <stack>
#include <algorithm>
#include <random>
#include <chrono>
#include "Timer.h"
#include "Settings.h"

using namespace std;
using namespace controller;

namespace view {

class MainWindow : public Fl_Window
{
    public:
        MainWindow(int width, int height, const char* title);
        virtual ~MainWindow();

        static void cbLetterTile(Fl_Widget* widget, void* data);
        static void cbTwist(Fl_Widget* widget, void* data);
        static void cbBackspace(Fl_Widget* widget, void* data);
        static void cbSubmit(Fl_Widget* widget, void* data);
        static void cbClear(Fl_Widget* widget, void* data);
        static void cbPlayAgain(Fl_Widget* widget, void* data);
        static void cbShowSettingsWindow(Fl_Widget* widget, void* data);
        static void cbShowScoresWindow(Fl_Widget* widget, void* data);

        void appendWordInputText(const string& outputText);
        void activateAllLetterTiles();
        void clearWordInputBox();
        string backspaceWordInputValue();
        void activateTileByLetter(string letterToFind);

        void pushToLetterTileStack(Fl_Button* button);
        Fl_Button* popLetterTileStack();
        void emptyLetterTileStack();
        void disableAllGameplayButtons();
        void enableAllGameplayButtons();



        static void cbTimer(void* data);


    private:
        Fl_Button* firstLetterButton;
        Fl_Button* secondLetterButton;
        Fl_Button* thirdLetterButton;
        Fl_Button* fourthLetterButton;
        Fl_Button* fifthLetterButton;
        Fl_Button* sixthLetterButton;
        Fl_Button* seventhLetterButton;

        Fl_Text_Buffer *wordInputTextBuffer;
        Fl_Text_Display *wordInputTextDisplay;
        Fl_Button* submitButton;
        Fl_Button* twistButton;
        Fl_Button* backspaceButton;
        Fl_Button* clearButton;

        Fl_Text_Buffer *messageTextBuffer;
        Fl_Text_Display *messageTextDisplay;

        Fl_Button* settingsButton;
        Fl_Button* playAgainButton;
        Fl_Button* viewScoresButton;

        Fl_Text_Buffer *timerTextBuffer;
        Fl_Text_Display *timerTextDisplay;
        Timer* gameTimer;

        GameManager* manager;
        stack<Fl_Button*> letterTileStack;

        vector<Fl_Button*> letterTileButtonsVector;
        Settings* gameSettings;

        void evaluateHighScore();

        void setupButtons();
        void redoButtonLabels();

};

}
#endif // MAINWINDOW_H
