#include "MainWindow.h"
#include <FL/fl_draw.H>
#include <iostream>
#include <array>
#include "SettingsWindow.h"
#include <HighScoreWindow.h>
#include "NewHighScoreWindow.h"
#include "HighScore.h"


namespace view {

/**
    Constructor for the main window of the application.
    @param width The width of the window
    @param height The height of the window
    @param title The title displayed in the top of the window
*/
MainWindow::MainWindow(int width, int height, const char* title) : Fl_Window(width, height, title)
{
    begin();

    this->manager = new GameManager();

    this->setupButtons();

    this->letterTileButtonsVector.push_back(this->firstLetterButton);
    this->letterTileButtonsVector.push_back(this->secondLetterButton);
    this->letterTileButtonsVector.push_back(this->thirdLetterButton);
    this->letterTileButtonsVector.push_back(this->fourthLetterButton);
    this->letterTileButtonsVector.push_back(this->fifthLetterButton);
    this->letterTileButtonsVector.push_back(this->sixthLetterButton);
    this->letterTileButtonsVector.push_back(this->seventhLetterButton);

    this->wordInputTextBuffer = new Fl_Text_Buffer();
    this->wordInputTextDisplay = new Fl_Text_Display(25, 120, 445, 50);
    this->wordInputTextDisplay->textfont(FL_COURIER);
    this->wordInputTextDisplay->buffer(wordInputTextBuffer);

    this->submitButton = new Fl_Button(475, 120, 70, 50, "Submit");
    this->submitButton->callback(cbSubmit, this);

    this->twistButton = new Fl_Button(175, 175, 100, 40, "Text Twist!");
    this->twistButton->callback(cbTwist, this);

    this->backspaceButton = new Fl_Button(280, 175, 100, 40, "Backspace");
    this->backspaceButton->callback(cbBackspace, this);

    this->clearButton = new Fl_Button(385, 175, 50, 40, "Clear");
    this->clearButton->callback(cbClear, this);

    this->messageTextBuffer = new Fl_Text_Buffer();
    this->messageTextDisplay = new Fl_Text_Display(175, 250, 350, 200);
    this->messageTextDisplay->textfont(FL_COURIER);
    this->messageTextDisplay->buffer(messageTextBuffer);
    this->messageTextDisplay->label("Output:");

    this->timerTextBuffer = new Fl_Text_Buffer();
    this->timerTextDisplay = new Fl_Text_Display(25, 175, 100, 40);
    this->timerTextDisplay->textfont(FL_COURIER);
    this->timerTextDisplay->buffer(timerTextBuffer);
    this->gameSettings = this->manager->getSettingsFromFile();
    this->gameTimer = new Timer(this->gameSettings->numberOfMinutesInGame);
    Fl::add_timeout(1, cbTimer, this);

    this->settingsButton = new Fl_Button(25, 360, 100, 40, "Settings");
    this->settingsButton->callback(cbShowSettingsWindow, this);
    this->viewScoresButton = new Fl_Button(25, 420, 100, 40, "View Scores");
    this->viewScoresButton->callback(cbShowScoresWindow, this);
    this->playAgainButton = new Fl_Button(25, 280, 100, 40, "Play Again?");
    this->playAgainButton->callback(cbPlayAgain, this);
    this->playAgainButton->hide();

    end();
}

void MainWindow::setupButtons()
{
    vector<string>* lettersForGame = this->manager->getLettersInPlay();
    const char* firstButtonValue = (*lettersForGame)[0].c_str();
    const char* secondButtonValue = (*lettersForGame)[1].c_str();
    const char* thirdButtonValue = (*lettersForGame)[2].c_str();
    const char* fourthButtonValue = (*lettersForGame)[3].c_str();
    const char* fifthButtonValue = (*lettersForGame)[4].c_str();
    const char* sixthButtonValue = (*lettersForGame)[5].c_str();
    const char* seventhButtonValue = (*lettersForGame)[6].c_str();

    this->firstLetterButton = new Fl_Button(25, 25, 70, 70, firstButtonValue);
    this->firstLetterButton->callback(cbLetterTile, this);

    this->secondLetterButton = new Fl_Button(100, 25, 70, 70, secondButtonValue);
    this->secondLetterButton->callback(cbLetterTile, this);

    this->thirdLetterButton = new Fl_Button(175, 25, 70, 70, thirdButtonValue);
    this->thirdLetterButton->callback(cbLetterTile, this);

    this->fourthLetterButton = new Fl_Button(250, 25, 70, 70, fourthButtonValue);
    this->fourthLetterButton->callback(cbLetterTile, this);

    this->fifthLetterButton = new Fl_Button(325, 25, 70, 70, fifthButtonValue);
    this->fifthLetterButton->callback(cbLetterTile, this);

    this->sixthLetterButton = new Fl_Button(400, 25, 70, 70, sixthButtonValue);
    this->sixthLetterButton->callback(cbLetterTile, this);

    this->seventhLetterButton = new Fl_Button(475, 25, 70, 70, seventhButtonValue);
    this->seventhLetterButton->callback(cbLetterTile, this);
}

/**
    Callback for a letter tile being pressed. Appends the letter that is in that tile to the textbox string.
    @param widget The widget that initiated the callback.
    @param data A reference to the MainWindow class passed.
*/
void MainWindow::cbLetterTile(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    if(!window->gameSettings->reuseLetters)
    {
        widget->deactivate();
    }
    window->pushToLetterTileStack((Fl_Button*) widget);
    window->appendWordInputText(widget->label());
}

/**
    Callback for the Text Twist! button being pressed. Randomly re-arranges the letters in the tiles.
    @param widget The widget that initiated the callback.
    @param data A reference to the MainWindow class passed.
*/
void MainWindow::cbTwist(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;

    vector<const char*> letterTileValuesToRandomize;

    for(int i = 0; i < 7; i++)
    {
        if(window->letterTileButtonsVector[i]->active())
        {
            letterTileValuesToRandomize.push_back(window->letterTileButtonsVector[i]->label());
        }
    }

    random_shuffle(letterTileValuesToRandomize.begin(), letterTileValuesToRandomize.end());

    for(int i = 0; i < letterTileValuesToRandomize.size(); i++)
    {
        if(window->letterTileButtonsVector[i]->active())
        {
            window->letterTileButtonsVector[i]->label(letterTileValuesToRandomize[i]);
        }
    }
}

/**
    Callback for the backspace button being pressed. Deletes the last letter on the end of the string in the textbox and re-activates the corresponding letter tile.
    @param widget The widget that initiated the callback.
    @param data A reference to the MainWindow class passed.
*/
void MainWindow::cbBackspace(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    if (!window->letterTileStack.empty())
    {
        window->backspaceWordInputValue();
        Fl_Button* latestButton = window->popLetterTileStack();
        latestButton->activate();
    }
}

/**
    Callback for the clear button being pressed. Clears all text in the textbox.
    @param widget The widget that initiated the callback.
    @param data A reference to the MainWindow class passed.
*/
void MainWindow::cbClear(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    window->clearWordInputBox();
    window->emptyLetterTileStack();
}

/**
    Callback for the submit button being pressed. Collects the word from the textbox then checks its validity then awards points appropriately.
    @param widget The widget that initiated the callback.
    @param data A reference to the MainWindow class passed.
*/
void MainWindow::cbSubmit(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    string textBoxValue = window->wordInputTextBuffer->text();
    window->clearWordInputBox();
    window->emptyLetterTileStack();
    string message = "";

    bool wordFound = window->manager->searchWord(textBoxValue);
    if(textBoxValue.size() < 3)
    {
        window->messageTextDisplay->color(FL_YELLOW);
        message = "Your word isn't long enough.\nMust be at least 3 characters.";
    }
    else if (window->manager->playerHasAlreadyEnteredWord(textBoxValue))
    {
        window->messageTextDisplay->color(FL_YELLOW);
        message = "You have already used that word!";
    }
    else if(wordFound)
    {
        window->messageTextDisplay->color(FL_GREEN);
        message = "You scored " + window->manager->scoreWord(textBoxValue) + " points with " + textBoxValue;
        window->manager->addWordEnterdByPlayer(textBoxValue);
        window->manager->scoreWordAndAddToCurrentScore(textBoxValue);
    }
    else
    {
        window->messageTextDisplay->color(FL_RED);
        message = textBoxValue + " isn't a word.\nYou lose 10 points!";
        window->manager->deductPointsForNonWord();
    }
    window->messageTextBuffer->text(message.c_str());
}

/**
    Sets the value of the word input textbox.
    @param outputText The text to set in the textbox.
*/
void MainWindow::appendWordInputText(const string& outputText)
{
    this->wordInputTextBuffer->append(outputText.c_str());
}

/**
    Activates all letter tile buttons.
*/
void MainWindow::activateAllLetterTiles()
{
    this->firstLetterButton->activate();
    this->secondLetterButton->activate();
    this->thirdLetterButton->activate();
    this->fourthLetterButton->activate();
    this->fifthLetterButton->activate();
    this->sixthLetterButton->activate();
    this->seventhLetterButton->activate();
}

/**
    Clears the word input text box.
*/
void MainWindow::clearWordInputBox()
{
    this->wordInputTextBuffer->text("");
    this->activateAllLetterTiles();
}

/**
    Deletes the last letter on the word input text buffer.
    @return the last letter of the word
*/
string MainWindow::backspaceWordInputValue()
{
    this->wordInputTextBuffer->remove(this->wordInputTextBuffer->length() - 1, this->wordInputTextBuffer->length());
}

/**
    Pushes a button to the stack that keeps track of the order that the tiles are pressed.
    @param button The button being pushed onto the stack.
*/
void MainWindow::pushToLetterTileStack(Fl_Button* button)
{
    this->letterTileStack.push(button);
}

/**
   Removes the top value from the stack and returns it.
*/
Fl_Button* MainWindow::popLetterTileStack()
{
    Fl_Button* buttonToReturn = this->letterTileStack.top();
    this->letterTileStack.pop();
    return buttonToReturn;
}

/**
   Removes the top value from the stack and returns it.
*/
void MainWindow::emptyLetterTileStack()
{
    while (!this->letterTileStack.empty())
    {
        this->letterTileStack.pop();
    }
}

/**
    Disables all buttons necessary for gameplay.
*/
void MainWindow::disableAllGameplayButtons()
{
    for(Fl_Button* button : this->letterTileButtonsVector)
    {
        button->deactivate();
    }

    this->submitButton->deactivate();
    this->backspaceButton->deactivate();
    this->clearButton->deactivate();
    this->twistButton->deactivate();
}

/**
    Enables all buttons necessary for gameplay.
*/
void MainWindow::enableAllGameplayButtons()
{
    for(Fl_Button* button : this->letterTileButtonsVector)
    {
        button->activate();
    }

    this->submitButton->activate();
    this->backspaceButton->activate();
    this->clearButton->activate();
    this->twistButton->activate();
}

/**
    Callback for the Timer behavior of texttwist.
*/
void MainWindow::cbTimer(void* data)
{
    MainWindow* window = (MainWindow*)data;
    window->gameTimer->tick();
    window->timerTextBuffer->text(window->gameTimer->getTime().c_str());
    if(window->gameTimer->finished())
    {
        window->messageTextDisplay->color(FL_WHITE);
        window->disableAllGameplayButtons();
        window->playAgainButton->show();
        window->messageTextBuffer->text(window->manager->getWordsEnteredByPlayer().c_str());
        window->evaluateHighScore();
    }
    else
    {
        Fl::repeat_timeout(1, cbTimer, data);
    }
}

void MainWindow::evaluateHighScore()
{
    if(this->manager->checkIsHighScore(this->manager->getPlayerScore()))
    {
        NewHighScoreWindow highScoreEntry;
        highScoreEntry.set_modal();
        highScoreEntry.show();

        while (highScoreEntry.shown())
        {
            Fl::wait();
        }
        if (highScoreEntry.getPlayerName().compare("") != 0)
        {
            HighScore* newHighScore = new HighScore(highScoreEntry.getPlayerName(), this->manager->getPlayerScore());
            this->manager->addHighScore(newHighScore);
        }
    }
}

/**
    Callback for handling the restart of the game.
*/
void MainWindow::cbPlayAgain(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    window->enableAllGameplayButtons();
    window->wordInputTextBuffer->text("");
    window->gameSettings = window->manager->getSettingsFromFile();
    window->gameTimer = new Timer(window->gameSettings->numberOfMinutesInGame);
    Fl::add_timeout(1, cbTimer, window);
    window->playAgainButton->hide();
    window->messageTextBuffer->text("");
    window->manager->clearPlayedWords();
    window->manager->populateLettersInPlayVector();
    window->redoButtonLabels();
}

void MainWindow::redoButtonLabels()
{
    vector<string>* lettersForGame = this->manager->getLettersInPlay();
    const char* firstButtonValue = (*lettersForGame)[0].c_str();
    const char* secondButtonValue = (*lettersForGame)[1].c_str();
    const char* thirdButtonValue = (*lettersForGame)[2].c_str();
    const char* fourthButtonValue = (*lettersForGame)[3].c_str();
    const char* fifthButtonValue = (*lettersForGame)[4].c_str();
    const char* sixthButtonValue = (*lettersForGame)[5].c_str();
    const char* seventhButtonValue = (*lettersForGame)[6].c_str();

    this->firstLetterButton->label(firstButtonValue);
    this->secondLetterButton->label(secondButtonValue);
    this->thirdLetterButton->label(thirdButtonValue);
    this->fourthLetterButton->label(fourthButtonValue);
    this->fifthLetterButton->label(fifthButtonValue);
    this->sixthLetterButton->label(sixthButtonValue);
    this->seventhLetterButton->label(seventhButtonValue);
}

/**
    Shows the settings window and imports any changed settings into the MainWindow.
*/
void MainWindow::cbShowSettingsWindow(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    SettingsWindow settings;
    settings.set_modal();
    settings.show();

    while (settings.shown())
    {
        Fl::wait();
    }

    if (settings.getWindowResult() == OKCancelWindow::WindowResult::OK)
    {
        window->gameSettings = settings.getSettings();
    }
}

void MainWindow::cbShowScoresWindow(Fl_Widget* widget, void* data)
{
    MainWindow* window = (MainWindow*)data;
    HighScoreWindow scoresWindow;
    scoresWindow.set_modal();
    scoresWindow.show();

    while (scoresWindow.shown())
    {
        Fl::wait();
    }
}

/**
    Destructor that cleans up all allocated resources for the window
*/
MainWindow::~MainWindow()
{
    delete this->firstLetterButton;
    delete this->secondLetterButton;
    delete this->thirdLetterButton;
    delete this->fourthLetterButton;
    delete this->fifthLetterButton;
    delete this->sixthLetterButton;
    delete this->seventhLetterButton;

    this->wordInputTextDisplay->buffer(0);
    delete this->wordInputTextBuffer;
    delete this->wordInputTextDisplay;

    delete this->submitButton;
    delete this->twistButton;
    delete this->backspaceButton;
    delete this->clearButton;

    delete this->settingsButton;
    delete this->playAgainButton;
    delete this->viewScoresButton;

    delete this->manager;

    this->messageTextDisplay->buffer(0);
    delete this->messageTextBuffer;
    delete this->messageTextDisplay;

    this->timerTextDisplay->buffer(0);
    delete this->timerTextBuffer;
    delete this->timerTextDisplay;
    delete this->gameTimer;
}

}
