#ifndef NEWHIGHSCOREWINDOW_H
#define NEWHIGHSCOREWINDOW_H

#include "OKCancelWindow.h"

//#include <Fl/Fl_Window.H>
//#include <Fl/Fl_Button.H>

#include <Fl/Fl_Input.H>
#include <Fl/fl_ask.H>

#include <string>
#include <sstream>

//using namespace std;

namespace view
{


class NewHighScoreWindow : public OKCancelWindow
{

private:
    Fl_Input* playerNameInput;
    std::string playerName;

    void validateInput();

public:
    NewHighScoreWindow();
    virtual ~NewHighScoreWindow();

    void okHandler();
    void cancelHandler();

    const std::string getPlayerName() const;

};

}

#endif // NEWHIGHSCOREWINDOW_H
