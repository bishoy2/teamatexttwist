#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <iostream>
#include <fstream>
#include <string>

namespace controller
{

class FileWriter
{
    private:
        std::string outputFile;
        std::ofstream fileStream;

    public:
        FileWriter(const std::string& outputFile);
        virtual ~FileWriter();

        void WriteOutput(const std::string& output);


};

}

#endif // FILEWRITER_H
