#ifndef DICTIONARYREADER_H
#define DICTIONARYREADER_H

#include "../model/Trie.h"
using namespace model;

#include "./FileReader.h"

namespace controller
{

class DictionaryReader
{
    private:

    public:
        DictionaryReader();
        virtual ~DictionaryReader();

        Trie* ReadInDictionary(const std::string& filePath);

};


}

#endif // DICTIONARYREADER_H
