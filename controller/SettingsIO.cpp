#include "SettingsIO.h"

#include "../Utils/Utils.h"

#define DEBUG
#ifdef DEBUG
    #include <iostream>
#endif // DEBUG

namespace controller
{

SettingsIO::SettingsIO()
{
    //ctor
}

SettingsIO::~SettingsIO()
{
    //dtor
}

/**
    Reads in settings from file system.
    If the settings file doesn't exixt, the default settings are returned.
*/
Settings* SettingsIO::readInSettings()
{
    Settings* settings;
    FileReader* fileReader = new FileReader(this->settingsFilePath);
    if (fileReader->checkFileExists())
    {
        std::vector<std::string> lines = fileReader->ReadFile();
        settings = this->parseSettingsFromLines(lines);

        #ifdef DEBUG
            std::cout << "Settings exist" << std::endl;
        #endif // DEBUG
    }
    else
    {
        settings = new Settings();

        #ifdef DEBUG
            std::cout << "Settings don't exist" << std::endl;
        #endif // DEBUG
    }

    delete fileReader;
    fileReader = 0;

    return settings;
}

/**
    Saves settings to file system
*/
void SettingsIO::saveSettings(Settings* settings)
{
    FileWriter* fileWriter = new FileWriter(this->settingsFilePath);
    std::stringstream outputStream;
    outputStream << "NumberOfMinutesInGame" << "=" << settings->numberOfMinutesInGame << endl;
    if (settings->reuseLetters)
    {
        outputStream << "ReuseLetters" << "=" << "True" << endl;
    }
    else
    {
        outputStream << "ReuseLetters" << "=" << "False" << endl;
    }

    fileWriter->WriteOutput(outputStream.str());
    delete fileWriter;
}


/**
    Parses a list of strings to read in settings.
*/
Settings* SettingsIO::parseSettingsFromLines(std::vector<std::string> settingsLines)
{
    int numberOfMinutesInGame;
    bool reuseLetters;
    for (std::string &lineValue : settingsLines)
    {
        if (lineValue != "")
        {
            std::vector<std::string> settingsValues;
            std::stringstream stream(lineValue);
            std::string value;
            while (getline(stream, value, '='))
            {
                settingsValues.push_back(value);
            }

            if (settingsValues.at(0) == "NumberOfMinutesInGame")
            {
                numberOfMinutesInGame = stoi(settingsValues.at(1));
            }
            else if (settingsValues.at(0) == "ReuseLetters")
            {
                if (toUpperCase(settingsValues.at(1)) == "TRUE")
                {
                    reuseLetters = true;
                }
                else if (toUpperCase(settingsValues.at(1)) == "FALSE")
                {
                    reuseLetters = false;
                }
            }
        }
    }
    Settings* savedSettings = new Settings(numberOfMinutesInGame, reuseLetters);
    return savedSettings;
}

}
