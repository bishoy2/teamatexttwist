#include "GameManager.h"
#include <cstdlib>
#include <algorithm>
#include <random>
#include <chrono>
#include <cmath>

#define DEBUG
#ifdef DEBUG
    #include <iostream>
#endif // DEBUG

namespace controller {

/**
    Constructor for the GameManager class.
*/
GameManager::GameManager()
{
    this->gameLetterList = new vector<string>();
    this->lettersInPlay = new vector<string>();
    this->populateLetterList();
    this->populateLettersInPlayVector();

    this->gameDictionaryTrie = new Trie();
    this->loadDictionary();

    this->validWordsEnteredByPlayer = new PlayerWords();

    this->currentPlayerScore = 0;

    this->highScoreManager = new HighScoreManager();
}

void GameManager::populateLetterList()
{
    for(int i = 0; i < 11; i++)
    {
        this->gameLetterList->push_back("E");
    }
    for(int i = 0; i < 9; i++)
    {
        this->gameLetterList->push_back("T");
    }
    for(int i = 0; i < 8; i++)
    {
        this->gameLetterList->push_back("O");
    }
    for(int i = 0; i < 6; i++)
    {
        this->gameLetterList->push_back("A");
        this->gameLetterList->push_back("I");
        this->gameLetterList->push_back("N");
        this->gameLetterList->push_back("S");
    }
    for(int i = 0; i < 5; i++)
    {
        this->gameLetterList->push_back("H");
        this->gameLetterList->push_back("R");
    }
    for(int i = 0; i < 4; i++)
    {
        this->gameLetterList->push_back("L");
    }
    for(int i = 0; i < 3; i++)
    {
        this->gameLetterList->push_back("D");
        this->gameLetterList->push_back("U");
        this->gameLetterList->push_back("W");
        this->gameLetterList->push_back("Y");
    }
    for(int i = 0; i < 2; i++)
    {
        this->gameLetterList->push_back("B");
        this->gameLetterList->push_back("C");
        this->gameLetterList->push_back("F");
        this->gameLetterList->push_back("G");
        this->gameLetterList->push_back("M");
        this->gameLetterList->push_back("P");
        this->gameLetterList->push_back("V");
    }
    this->gameLetterList->push_back("J");
    this->gameLetterList->push_back("K");
    this->gameLetterList->push_back("Q");
    this->gameLetterList->push_back("X");
    this->gameLetterList->push_back("Z");
}

/**
    Returns the vector that contains the 7 random letters for play.
*/
vector<string>* GameManager::getLettersInPlay()
{
    return this->lettersInPlay;
}

/**
    Populates letters for the game.
*/
void GameManager::populateLettersInPlayVector()
{
    if (!this->lettersInPlay->empty())
    {
        this->lettersInPlay->clear();
    }

    srand(time(NULL));

    int randomNumber1 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand1 " << randomNumber1 << std::endl;
    #endif // DEBUG
    int randomNumber2 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand2 " << randomNumber2 << std::endl;
    #endif // DEBUG
    int randomNumber3 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand3 " << randomNumber3 << std::endl;
    #endif // DEBUG
    int randomNumber4 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand4 " << randomNumber4 << std::endl;
    #endif // DEBUG
    int randomNumber5 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand5 " << randomNumber5 << std::endl;
    #endif // DEBUG
    int randomNumber6 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand6 " << randomNumber6 << std::endl;
    #endif // DEBUG
    int randomNumber7 = rand() % this->gameLetterList->size();
    #ifdef DEBUG
        std::cout << "rand7 " << randomNumber7 << std::endl;
    #endif // DEBUG

    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber1]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber2]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber3]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber4]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber5]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber6]);
    this->lettersInPlay->push_back((*this->gameLetterList)[randomNumber7]);

}

/**
    Loads the dictionary for the game.
*/
void GameManager::loadDictionary()
{
    delete this->gameDictionaryTrie;
    DictionaryReader* reader = new DictionaryReader();
    this->gameDictionaryTrie = reader->ReadInDictionary("./resources/dictionary.txt");
    delete reader;
}

/**
    Searches the dictionary for a word.
*/
bool GameManager::searchWord(string word)
{
    return this->gameDictionaryTrie->searchWord(word);
}

/**
    Gets the settings from the file where they are stored.
*/
Settings* GameManager::getSettingsFromFile()
{
    SettingsIO* settingsFile = new SettingsIO();
    Settings* gameSettings = settingsFile->readInSettings();
    delete settingsFile;
    return gameSettings;
}

/**
    Gets the words entered by the player.
*/
string GameManager::getWordsEnteredByPlayer()
{
    return this->validWordsEnteredByPlayer->getAllWords();
}

/**
    Adds the words entered by the player.
*/
void GameManager::addWordEnterdByPlayer(string word)
{
    this->validWordsEnteredByPlayer->addWord(word);
}

bool GameManager::playerHasAlreadyEnteredWord(string word)
{
    return this->validWordsEnteredByPlayer->contains(word);
}

/**
    Calls HighScoreManager method 'checkIsHighScore'
*/
bool GameManager::checkIsHighScore(int score)
{
    return this->highScoreManager->checkIsHighScore(score);
}

/**
    Calls HighScoreManager method 'addHighScore'
*/
void GameManager::addHighScore(HighScore* highScore)
{
    this->highScoreManager->addHighScore(highScore);
}

/**
    Scores the word and adds it to the current player score.
    @param word The word being evaluated.
*/
void GameManager::scoreWordAndAddToCurrentScore(string word)
{
    int score = 0;
    int wordLength = word.size();
    score = ((int)pow(wordLength, 2)) * 10;
    this->currentPlayerScore += score;
}

/**
    Scores the word and returns it.
    @param word The word being evaluated.
    @return the word's score
*/
string GameManager::scoreWord(string word)
{
    return to_string((int)pow(word.size(), 2) * 10);
}

/**
    Returns the player's current score.
*/
int GameManager::getPlayerScore()
{
    return this->currentPlayerScore;
}

/**
    Deducts 10 points from player score for entering a wrong word.
*/
void GameManager::deductPointsForNonWord()
{
    this->currentPlayerScore -= 10;
}

/**
    Clears played words.
*/
void GameManager::clearPlayedWords()
{
    this->validWordsEnteredByPlayer->clearWords();
}

/**
    Destructor that cleans up all allocated resources for the GameManager
*/
GameManager::~GameManager()
{
    delete this->gameLetterList;
    delete this->lettersInPlay;
    delete this->gameDictionaryTrie;
}


}
