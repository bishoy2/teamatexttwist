#ifndef FILEREADER_H
#define FILEREADER_H

#include <string>
#include <vector>
#include <fstream>

namespace controller
{

class FileReader
{
    protected:
        std::string filePath;

    public:
        FileReader(const std::string& filePath);
        virtual ~FileReader();

        std::vector<std::string> ReadFile();

        bool checkFileExists();

    private:
};

}

#endif // FILEREADER_H
