#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <string>
#include <vector>
#include <array>
#include "Settings.h"

using namespace std;

#include <HighScoreManager.h>
#include <Trie.h>
using namespace model;

#include <SettingsIO.h>
#include "./DictionaryReader.h"

#include "PlayerWords.h"

namespace controller {

class GameManager
{
    public:
        GameManager();
        virtual ~GameManager();

        vector<string>* getLettersInPlay();

        void loadDictionary();
        bool searchWord(string word);

        Settings* getSettingsFromFile();

        string getWordsEnteredByPlayer();
        void addWordEnterdByPlayer(string word);

        bool checkIsHighScore(int score);
        void addHighScore(HighScore* highScore);

        void scoreWordAndAddToCurrentScore(string word);
        string scoreWord(string word);
        int getPlayerScore();

        bool playerHasAlreadyEnteredWord(string word);

        void deductPointsForNonWord();

        void populateLettersInPlayVector();

        void clearPlayedWords();

    private:
        Trie* gameDictionaryTrie;

        vector<string>* gameLetterList;
        void populateLetterList();

        vector<string>* lettersInPlay;


        int const numberOfRandomLetters = 7;

        PlayerWords* validWordsEnteredByPlayer;
        int currentPlayerScore;

        HighScoreManager* highScoreManager;
};

}
#endif // GAMEMANAGER_H
