#include "FileWriter.h"

#define DEBUG
#ifdef DEBUG
    #include <iostream>
#endif // DEBUG

namespace controller
{

/**
    Constructor for FileWriter
*/
FileWriter::FileWriter(const std::string& outputFile)
{
    this->outputFile = outputFile;
}

/**
    Destructor for FileWriter
*/
FileWriter::~FileWriter()
{
    //dtor
}

/**
    Writes the output to the console.  If WriteToFile is enabled it also writes to the file path configured.
    @param output the text to be written to file.
*/
void FileWriter::WriteOutput(const std::string& output)
{
    #ifdef DEBUG
        std::cout << output << std::endl;
    #endif // DEBUG

    this->fileStream.open(this->outputFile, std::ofstream::out | std::ofstream::trunc);
    this->fileStream << output;
    this->fileStream.close();
}


}
