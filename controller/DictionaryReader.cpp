#include "DictionaryReader.h"

#define DEBUG
#ifdef DEBUG
    #include <algorithm>

#endif // DEBUG


namespace controller
{

/**
    Constructor for DictionaryReader
*/
DictionaryReader::DictionaryReader()
{
    //ctor
}

/**
    Destructor for DictionaryReader
*/
DictionaryReader::~DictionaryReader()
{
    //dtor
}

/**
    Returns a Trie constructed from the dictionary file.
*/
Trie* DictionaryReader::ReadInDictionary(const std::string& filePath)
{
    Trie* dictionaryTrie = new Trie();
    FileReader* fileReader = new FileReader(filePath);
    vector<std::string> dictionaryWords = fileReader->ReadFile();

    for (string &lineValue : dictionaryWords)
    {
        if (lineValue != "")
        {
            while ( lineValue.find ("\r") != string::npos )
            {
                lineValue.erase ( lineValue.find ("\r"), 1 );
            }

            #ifdef DEBUG
                cout << "Adding: " << lineValue << endl;
            #endif // DEBUG
            dictionaryTrie->addWord(lineValue);
        }
    }
    return dictionaryTrie;
}

}
