#ifndef HIGHSCOREFILEIO_H
#define HIGHSCOREFILEIO_H

#include <vector>
#include <sstream>

#include <HighScore.h>
using namespace model;

#include <FileReader.h>
#include <FileWriter.h>

namespace controller
{

class HighScoreFileIO
{
    private:
        const std::string highScoresFilePath = "./resources/HighScores.csv";


        HighScore* createHighScoreFromLine(const std::string& line);

    public:
        HighScoreFileIO();
        virtual ~HighScoreFileIO();

        std::vector<HighScore*> readInHighScores();
        void saveHighScores(std::vector<HighScore*> highScores);



};

}

#endif // HIGHSCOREFILEIO_H
