#ifndef SETTINGSIO_H
#define SETTINGSIO_H

#include <string>
#include <vector>
#include <sstream>

#include <Settings.h>
using namespace model;

#include <FileReader.h>
#include <FileWriter.h>

namespace controller
{

class SettingsIO
{
    private:
        Settings* parseSettingsFromLines(std::vector<std::string> settingsLines);

        const std::string settingsFilePath = "./resources/settings.ini";

    public:
        SettingsIO();
        virtual ~SettingsIO();

        Settings* readInSettings();
        void saveSettings(Settings* settings);

};

}
#endif // SETTINGSIO_H
