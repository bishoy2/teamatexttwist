#include "HighScoreFileIO.h"

//#define DEBUG
#ifdef DEBUG
    #include <iostream>
#endif // DEBUG

namespace controller
{

/**
    Constructor for HighScoreFileIO
*/
HighScoreFileIO::HighScoreFileIO()
{
    //ctor
}

/**
    Destructor for HighScoreFileIO
*/
HighScoreFileIO::~HighScoreFileIO()
{
    //dtor
}

/**
    Reads in the HighScore objects saved to file.
    @return a vector of HighScore objects loaded from file.
*/
std::vector<HighScore*> HighScoreFileIO::readInHighScores()
{
    std::vector<HighScore*> highScores;

    FileReader* fileReader = new FileReader(this->highScoresFilePath);
    if (fileReader->checkFileExists())
    {
        std::vector<std::string> lines = fileReader->ReadFile();
        for (std::string &lineValue : lines)
        {
            if (lineValue != "")
            {
                HighScore* newHighScore = this->createHighScoreFromLine(lineValue);
                highScores.push_back(newHighScore);
            }
        }
    }

    delete fileReader;
    fileReader = 0;
    return highScores;
}

/**
    Parses the line and creates a new HighScore object.
    @param line the line to be parsed.
    @return a HighScore object created from the line.
*/
HighScore* HighScoreFileIO::createHighScoreFromLine(const std::string& line)
{
    std::vector<std::string> highScoreValues;

    std::stringstream stream(line);
    std::string value;
    while (getline(stream, value, ','))
    {
        highScoreValues.push_back(value);
    }

    std::string name = highScoreValues.at(0);
    int score = stoi(highScoreValues.at(1));

    HighScore* highScore = new HighScore(name, score);
    return highScore;
}

/**
    Saves the HighScore objects to file.
    @param highScores the HighScore objects to save to file.
*/
void HighScoreFileIO::saveHighScores(std::vector<HighScore*> highScores)
{
    FileWriter* fileWriter = new FileWriter(this->highScoresFilePath);
    std::stringstream outputStream;
    for (HighScore* highScore: highScores)
    {
        outputStream << highScore->getName() << "," << highScore->getScore() << std::endl;
    }
    fileWriter->WriteOutput(outputStream.str());
    delete fileWriter;
}

}
