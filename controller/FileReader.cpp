#include "FileReader.h"

namespace controller
{

/**
    Constructor for FileReader
*/
FileReader::FileReader(const std::string& filePath)
{
    this->filePath = filePath;
}

/**
    Destructor for FileReader
*/
FileReader::~FileReader()
{
    //dtor
}

/**
    Returns a vector of lines of the file contents.
    @return a vector of lines of the file contents.
*/
std::vector<std::string> FileReader::ReadFile()
{
    std::vector<std::string> lines;

    std::ifstream file ( this->filePath );
    std::string value;
    while ( file.good() )
    {
        getline ( file, value);
        lines.push_back(value);
    }

    return lines;
}

/**
    Returns true if the file exists
    @return returns true if the file exists.
*/
bool FileReader::checkFileExists()
{
    if (this->filePath.compare("") != 0)
    {
        std::ifstream infile(filePath);
        return infile.good();
    }
    return false;
}

}
