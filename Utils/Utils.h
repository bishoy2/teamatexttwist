#ifndef UTILS_H
#define UTILS_H

#include <string>
using namespace std;

const string toUpperCase(string text);

#endif // UTILS_H
