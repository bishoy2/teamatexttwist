#ifndef SETTINGS_H
#define SETTINGS_H

namespace model {

class Settings
{
    private:

    public:
        Settings();
        Settings(int numberOfMinutesInGame, bool reuseLetters);
        virtual ~Settings();

        int numberOfMinutesInGame;
        bool reuseLetters;

};
}
#endif // SETTINGS_H
