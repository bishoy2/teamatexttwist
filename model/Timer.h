#ifndef TIMER_H
#define TIMER_H
#include <string>

using namespace std;

namespace model {

class Timer
{
    public:
        Timer(int minutes);
        virtual ~Timer();

        string getTime();
        void tick();
        bool finished();

    private:
        int seconds;
        int minutes;
};

}
#endif // TIMER_H
