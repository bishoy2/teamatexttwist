#ifndef PLAYERWORDS_H
#define PLAYERWORDS_H

#include <vector>
#include <string>

using namespace std;

namespace model {

class PlayerWords
{
    public:
        PlayerWords();
        virtual ~PlayerWords();

        void addWord(string word);
        string getAllWords();
        bool contains(string word);
        void clearWords();

    private:
        vector<string> words;
};
}
#endif // PLAYERWORDS_H
