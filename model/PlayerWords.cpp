#include "PlayerWords.h"
#include <sstream>
#include <algorithm>

namespace model {

/**
    Constructor for the PlayerWords object.
*/
PlayerWords::PlayerWords()
{

}

/**
    Adds a valid word that the player has entered.
    @param word The word being added.
*/
void PlayerWords::addWord(string word)
{
    this->words.push_back(word);
}

/**
    Returns a string that contains all the words that the player ented through the course of the game.
*/
string PlayerWords::getAllWords()
{
    stringstream stream;
    for(string word : this->words)
    {
        stream << word << endl;
    }
    return stream.str();
}

/**
    Returns true if the word has already been enterd by the player.
    @param word The word being evaluated.
*/
bool PlayerWords::contains(string word)
{
    return std::find(this->words.begin(), this->words.end(), word) != this->words.end();
}

/**
    Clears played words.
*/
void PlayerWords::clearWords()
{
    this->words.clear();
}

PlayerWords::~PlayerWords()
{
    //dtor
}

}
