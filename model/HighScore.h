#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include <string>
#include <sstream>
#include <iomanip>

namespace model
{

class HighScore
{
    private:
        std::string name;
        int score;

    public:
        HighScore(const std::string& name, int score);
        virtual ~HighScore();

        const std::string& getName() const;
        int getScore() const;

        const std::string toString() const;

        static bool SortByScore(const HighScore* highScoreA, const HighScore* highScoreB);
        static bool SortByScoreDesc(const HighScore* highScoreA, const HighScore* highScoreB);
};

}

#endif // HIGHSCORE_H
