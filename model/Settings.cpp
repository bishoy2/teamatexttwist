#include "Settings.h"

namespace model {


/**
    Default constructor for the Settings class.
*/
Settings::Settings()
{
    this->numberOfMinutesInGame = 1;
    this->reuseLetters = false;
}

/**
    Constructor for the Settings class.
*/
Settings::Settings(int numberOfMinutesInGame, bool reuseLetters)
{
    this->numberOfMinutesInGame = numberOfMinutesInGame;
    this->reuseLetters = reuseLetters;
}

Settings::~Settings()
{
    //dtor
}

}
