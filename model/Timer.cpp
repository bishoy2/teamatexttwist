#include "Timer.h"

namespace model {

/**
    Constructor for the Timer class. Initializes minutes and seconds to 0.
*/
Timer::Timer(int minutes)
{
    this->seconds = 0;
    this->minutes = minutes;
}

/**
    Returns the string format of the current time on the Timer.
*/
string Timer::getTime()
{
    string timeString = "";

    timeString += to_string(this->minutes);
    string secondString = to_string(this->seconds);
    if(secondString.size() == 1)
    {
        timeString += ":0" + secondString;
    }
    else
    {
        timeString += ":" + secondString;
    }

    return timeString;
}

/**
    Increments the Timer down.
*/
void Timer::tick()
{
    if(this->minutes > 0 && this->seconds == 0)
    {
        this->seconds = 59;
        this->minutes -= 1;
    }
    else
    {
        this->seconds -= 1;
    }
}

/**
    Returns true if the timer is done counting down, and false otherwise.
*/
bool Timer::finished()
{
    return this->minutes == 0 && this->seconds == 0;
}

/**
    Destructor that cleans up all allocated resources for the Timer.
*/
Timer::~Timer()
{

}

}
