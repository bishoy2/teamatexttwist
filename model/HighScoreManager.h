#ifndef HIGHSCOREMANAGER_H
#define HIGHSCOREMANAGER_H

#include <vector>
#include <string>
#include <algorithm>

#include <HighScore.h>

#include <HighScoreFileIO.h>
using namespace controller;


namespace model
{

class HighScoreManager
{
    private:
        std::vector<HighScore*> highScores;

        const int maxNumberOfHighScores = 5;

    public:
        HighScoreManager();
        virtual ~HighScoreManager();

        void loadHighScores();
        bool checkIsHighScore(int score);
        void addHighScore(HighScore* highScore);
        std::vector<HighScore*> getHighScores();
        std::string getHighScoreText();

};

}

#endif // HIGHSCOREMANAGER_H
