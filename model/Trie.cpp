#include "Trie.h"
#include "../Utils/Utils.h"

namespace model
{

/**
    Trie implementation based in part on code from https://gist.github.com/reterVision/8487831
*/

/**
    Searched the trie for the character provided
    @param c the character to find
*/
Node* Node::findChild(char c)
{
    for ( int i = 0; i < mChildren.size(); i++ )
    {
        Node* tmp = mChildren.at(i);
        if ( tmp->content() == c )
        {
            return tmp;
        }
    }

    return NULL;
}

/**
    Constructor for this object.
*/
Trie::Trie()
{
    root = new Node();
}

/**
    Destructor for this object
*/
Trie::~Trie()
{
    // Free memory
}

/**
    Adds a word to this Trie
    @param s the word to add to this Trie
*/
void Trie::addWord(string s)
{
    s = toUpperCase(s);
    Node* current = root;

    if ( s.length() == 0 )
    {
        current->setWordMarker(); // an empty word
        return;
    }

    for ( int i = 0; i < s.length(); i++ )
    {
        Node* child = current->findChild(s[i]);
        if ( child != NULL )
        {
            current = child;
        }
        else
        {
            Node* tmp = new Node();
            tmp->setContent(s[i]);
            current->appendChild(tmp);
            current = tmp;
        }
        if ( i == s.length() - 1 )
            current->setWordMarker();
    }
}

/**
    Searches this Trie for the provided word
    @param s the word to search for.
*/
bool Trie::searchWord(string s)
{
    Node* current = root;

    while ( current != NULL )
    {
        for ( int i = 0; i < s.length(); i++ )
        {
            Node* tmp = current->findChild(s[i]);
            if ( tmp == NULL )
                return false;
            current = tmp;
        }

        if ( current->wordMarker() )
            return true;
        else
            return false;
    }

    return false;
}

}

