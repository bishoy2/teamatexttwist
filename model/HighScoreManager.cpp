#include "HighScoreManager.h"

namespace model
{

/**
    Initializes this object
*/
HighScoreManager::HighScoreManager()
{

}

/**
    Destructor for this object
*/
HighScoreManager::~HighScoreManager()
{

}

/**
    Loads the HighScore values that are saved to the file system.

    @precondition none
    @postcondition getHighScores() will return high scores that have been saved to file.
*/
void HighScoreManager::loadHighScores()
{
    HighScoreFileIO* highScoreFile = new HighScoreFileIO();
    this->highScores = highScoreFile->readInHighScores();
}

/**
    Checks the provided score to see if it is a high score.
    @param score the score to be checked for a high score.
*/
bool HighScoreManager::checkIsHighScore(int score)
{
    bool isHighScore = false;
    if (this->highScores.empty())
    {
        this->loadHighScores();
    }
    if (this->highScores.size() > 0)
    {
        std::sort(this->highScores.begin(), this->highScores.end(), HighScore::SortByScore);
        if (score > this->highScores.at(0)->getScore() || this->highScores.size() < this->maxNumberOfHighScores)
        {
            isHighScore = true;
        }
        else
        {
            isHighScore = false;
        }
    }
    else
    {
        isHighScore = true;
    }
    return isHighScore;
}

/**
    Adds a new HighScore to this collection.
    @precondition none
    @postcondition this->getHighScores.size()++
*/
void HighScoreManager::addHighScore(HighScore* highScore)
{
    this->highScores.push_back(highScore);
    while(this->highScores.size() > this->maxNumberOfHighScores)
    {
        std::sort(this->highScores.begin(), this->highScores.end(), HighScore::SortByScore);
        this->highScores.erase(this->highScores.begin() + 0);
    }
    HighScoreFileIO* highScoreFile = new HighScoreFileIO();
    highScoreFile->saveHighScores(this->highScores);
    delete highScoreFile;
}

/**
    Returns HighScores in this manager.
*/
std::vector<HighScore*> HighScoreManager::getHighScores()
{
    if (this->highScores.empty())
    {
        this->loadHighScores();
    }
    return this->highScores;
}

/**
    Returns a formatted string representation for the HighScore
    values in this collection
*/
std::string HighScoreManager::getHighScoreText()
{
    std::stringstream outputStream;
    std::sort(this->highScores.begin(), this->highScores.end(), HighScore::SortByScoreDesc);
    for (HighScore* highScore : this->highScores)
    {
        outputStream << highScore->toString() << std::endl;
    }
    return outputStream.str();
}

}
