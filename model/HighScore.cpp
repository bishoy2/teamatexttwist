#include "HighScore.h"

namespace model
{

/**
    Initializes this object
*/
HighScore::HighScore(const std::string& name, int score)
{
    this->name = name;
    this->score = score;
}


/**
    Destructor for this object
*/
HighScore::~HighScore()
{
    //dtor
}

/**
    Returns the player name
*/
const std::string& HighScore::getName() const
{
    return this->name;
}

/**
    Returns the score
*/
int HighScore::getScore() const
{
    return this->score;
}

/**
    Returns a string representation of this HighScore object
*/
const std::string HighScore::toString() const
{
    int columnWidth = 20;
    std::stringstream outputStream;
    outputStream << std::left << std::setw(columnWidth) << this->getName() << std::left << std::setw(columnWidth) << this->getScore();
    return outputStream.str();
}

/**
    Sorts HighScore objects by score ascending.
*/
bool HighScore::SortByScore(const HighScore* highScoreA, const HighScore* highScoreB)
{
    return highScoreA->getScore() < highScoreB->getScore();
}

/**
    Sorts HighScore objects by score descending.
*/
bool HighScore::SortByScoreDesc(const HighScore* highScoreA, const HighScore* highScoreB)
{
    return highScoreA->getScore() > highScoreB->getScore();
}

}
