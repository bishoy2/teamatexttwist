#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include "view/MainWindow.h"

using namespace view;

int main (int argc, char ** argv)
{
    MainWindow mainWindow(600, 475, "Team A TextTwist");
    mainWindow.show();

    int exitCode = Fl::run();
    return exitCode;
}
